<?php
/**
 * Сервис "Работа со столбцами". Рабочая часть, под заголовком и описанием.
 *
 */
?>


<div class="card-divider">
<div class="row">
    <div class="small-3 large-3 columns">
        <label class="toCol1">
            Столбец A
            <textarea id="1List" placeholder="Вставьте слова построчно или через запятую" rows="10" autofocus></textarea>
        </label>
    </div>
    <div class="small-3 large-3 columns">
        <label class="toCol2">
            Столбец B
                <textarea id="2List" placeholder="Вставьте слова построчно или через запятую" rows="10"></textarea>
        </label>
    </div>
    <div class="small-3 large-3 columns">
        <label class="toCol3">
            Столбец C
                <textarea id="3List" placeholder="Вставьте слова построчно или через запятую" rows="10"></textarea>
        </label>
    </div>
    <div class="small-3 large-3 columns">
        <label class="toCol4">
              Столбец D
                <textarea id="4List" placeholder="Вставьте слова построчно или через запятую" rows="10"></textarea>
        </label>
    </div>
</div>
</div>

<div class="expanded button-group">
    <button id="buttonResult" type="button" class="primary button">Перемножить</button>
    <button id="sampleResult" type="button" class="primary button">Для теста</button>
    <button id="btn-save" type="button" class="primary button">Сохранить файл</button>
    <button id="buttonmounth" type="button" class="primary button">Внешние данных</button>
    <button id="btnCols" value="4" type="button" class="primary button">Добавить + (4)</button>

</div>

<input type="text" class="form-control" id="input-fileName" value="freewords_words" placeholder="Введите название файла">

<div class="card-divider">
<div class="row">
        <div class="small-4 large-4 columns">
        <label>
            Результатик
            <textarea id="ResultList" placeholder="Вставьте слова построчно или через запятую" rows="10" autofocus></textarea>
        </label>
        </div>
</div>
</div>  
<table id="users">
    <tr><td>Id</td><td>Имя</td><td>Возраст</td><tr>
</table>


<div class="card-divider">
<div class="row">
        <div class="small-4 large-4 columns">
        <label>Количество фраз: <a id="colfraz"></a></label>
        </div>
</div>
</div>  

<script src="http://freewords.ru/wp-includes/js/FileSaver.js"></script>
<script type="text/javascript">
$("#sampleResult").on("click", function(){
    var strMounthOne= ["апрель"+"\n"+"май"+"\n"+"июнь"+"\n"+"январь"+"\n"];
    document.getElementById("1List").value = strMounthOne;
    var strMounthTwo= ["гулять"+"\n"+"получать деньги"+"\n"+"супер"+"\n"];
    document.getElementById("2List").value = strMounthTwo;
 var strMounth3= ["рыба"+"\n"+"пеллеты"+"\n"+"кошка"+"\n"+"собака"+"\n"];
    document.getElementById("3List").value = strMounth3;
     var strMounth4= ["freewords"+"\n"+"polik"+"\n"+"lenta.ru"+"\n"+"rbc.ru"+"\n"+"Oleg"+"\n"+"seminar"+"\n"];
    document.getElementById("4List").value = strMounth4;
});
// Только первая буква в строке
$("#btnCols").on("click", function(){
    var ert=$('[id$= List]');
    var ttt= document.getElementById('btnCols').value;
        //console.log(ttt);
        if (ttt<8) {
            $('.toCol1').append('Столбец E<textarea id="5List" placeholder="Вставьте слова построчно или через запятую" rows="10"></textarea>');
            $('.toCol2').append('Столбец F<textarea id="6List" placeholder="Вставьте слова построчно или через запятую" rows="10"></textarea>');
            $('.toCol3').append('Столбец G<textarea id="7List" placeholder="Вставьте слова построчно или через запятую" rows="10"></textarea>');
            $('.toCol4').append('Столбец H<textarea id="8List" placeholder="Вставьте слова построчно или через запятую" rows="10"></textarea>');
            document.getElementById('btnCols').value=8;

            //var kkk=;
            //var numberkkk=String(kkk)+"List";
            //document.getElementById('btnCols').value=8;
            //document.getElementById(numberkkk).value="slkjsklgjskgjgjs";
        };
});


$("#buttonmounth").on("click", function(){
    $.getJSON('http://freewords.ru/wp-includes/js/metro_Moscow.json', function(data) {
            for(var i=0;i<data.users.length;i++){
                $('#users').append('<tr><td>' + data.users[i].id + '</td><td>' + data.users[i].name + 
                '</td><td>' + data.users[i].age + '</td><tr>');
            }
    });
});


$("#buttonResult").on("click", function(){
    var FirstList = document.getElementById("1List").value.split(/[\n\r]+/); // Массив первого столбца
    var SecondList = document.getElementById("2List").value.split(/[\n\r]+/); // Массив второго столбца

    var TreeList = document.getElementById("3List").value.split(/[\n\r]+/); // Массив второго столбца
    var FourthList = document.getElementById("4List").value.split(/[\n\r]+/); // Массив второго столбца


    //var ThirdList = document.getElementById("SecondList").value.split(/[\n\r]+/); // Массив второго столбца
    var listResult = "";
    var colfraz=0;

    console.log(FirstList.length);
    console.log(SecondList.length);

    for (var i = 0; i < FirstList.length; i++) {
        if (FirstList[i] != "") {
            for (var t=0; t< SecondList.length; t++) {
                if (SecondList[t] != "") {                
                    for (var q=0; q< TreeList.length; q++) {
                        if (TreeList[q] != "") {
                            for (var z=0; z< FourthList.length; z++) {
                                if (FourthList[z] != "") {
                                    colfraz=colfraz+1;
                                    listResult = listResult + FirstList[i].trim() + " " + SecondList[t] + " "+TreeList[q]+ " "+FourthList[z]+"\r\n";
                }   
            }
        }
    }}}}}
    console.log(colfraz);
        document.getElementById("ResultList").value = listResult;
        $("#colfraz").text(colfraz);
});

$("#btn-save").click( function() {


  var text = $("#ResultList").val();
  var filename = $("#input-fileName").val();
  var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
  saveAs (blob, filename+".txt");
});





</script>