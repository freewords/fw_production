<?php
/**
 * Сервис "Заглавная буква". Рабочая часть, под заголовком и описанием.
 *
 */
?>


<br>
<div class="row columns">
<div class="row">
        <div class="large-6 columns">
            <div style="display: inline-block"><input id="checkboxSpace" type="checkbox" checked><label for="checkboxSpace" text-right middle>Убрать пустые строки</label></div>
            <div style="display: inline-block"><input id="checkboxLetter" type="checkbox"><label for="checkboxLetter">Каждое слово с заглавной</label></div>
        </div>
</div>
    <div class="row">
        <div class="small-12 medium-6 columns">
            <label>
                Исходный список
                <textarea id="listSource" placeholder="Вставьте список слов построчно" rows="20" autofocus></textarea>
            </label>
        </div>
        <div class="small-12 medium-6 columns"><label>
                Результат
                <textarea id="listResult" placeholder="" rows="20" disabled></textarea>
                </label>
        </div>

        <div class="small-12 large-12 large-centered columns">
            <div style="display: block"><button id="buttonResult" type="button" class="success button" oncslick="listUpper()">Сделать заглавными</button></div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#buttonResult").on("click", function(){

    var listSource      = document.getElementById("listSource");
    var listResult      = "";
    var checkboxSpace   = document.getElementById('checkboxSpace');
    var checkboxLetter  = document.getElementById('checkboxLetter');

    if ( checkboxSpace.checked ) {
        listSource = listSource.value.split(/[\n\r]+/);
    } else {
        listSource = listSource.value.split(/[\n\r]/);
    }

    if ( checkboxLetter.checked ) {
        // Все слова с заглавной
        for (var i = 0; i < listSource.length; i++) {
            listResult = listResult + listSource[i].replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); }) + "\r\n";
        }
    } else {
        // Только первые буквы фразы с заглавной
        for (var i = 0; i < listSource.length; i++) {
            listResult = listResult + listSource[i].substr(0,1).toUpperCase() + listSource[i].slice(1) + "\r\n";
        }

    }

    document.getElementById("listResult").value = listResult;
    document.getElementById("listResult").disabled = false;
    
});


</script>


                   
