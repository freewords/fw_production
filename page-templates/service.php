<?php
/*
Template Name: Сервис
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<div id="page-full-width" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">

      

      <div class="row"> 
        <div class="columns large-6">
          <header>
              <h1 contenteditable="true" class="entry-title"><?php the_title(); ?></h1>
          </header>
        </div>
        <div class="columns large-6">
          <div class="entry-content">
          <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
          <?php the_content(); ?>
          </div>
        </div> 
      </div>

          


           <!-- Подключение сервиса -->
           <?php  
                global $post;
                $post_slug=$post->post_name;
                get_template_part('services/service', $post_slug); 

            ?>

          <!--  -->

           <?php edit_post_link( __( 'Edit', 'foundationpress' ), '<span class="edit-link">', '</span>' ); ?>

      <footer>
          <?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
          <p><?php the_tags(); ?></p>
      </footer>
      <?php do_action( 'foundationpress_page_before_comments' ); ?>
      <?php comments_template(); ?>
      <?php do_action( 'foundationpress_page_after_comments' ); ?>
  </article>
<?php endwhile;?>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer();
